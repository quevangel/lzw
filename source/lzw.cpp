#include "lzw.hpp"

#include <algorithm>
 
#include <print.hpp>

LZW Compress_(std::ifstream& input)
{
    char current_char = '\0'; 
    std::vector<std::string> dictionary = {};
    std::vector<bool> binary_output = {};
    unsigned bits_per_code = 0;

    while (input.get(current_char))
        if (std::find(dictionary.begin(), dictionary.end(), std::string(1, current_char)) == dictionary.end())
            dictionary.push_back(std::string(1, current_char));
    input.clear();
    input.seekg(0);

    std::vector<std::string> original_dictionary(dictionary.begin(), dictionary.end());

    bits_per_code = Required_No_Bits_(dictionary.size() - 1);
    std::cout << "Número de cadenas con longitud 1 = " << dictionary.size() << "\n";
    std::cout << "Dígitos requeridos por código = " << bits_per_code << "\n";

    std::string current_string = "";
    int output_count = 0, input_count = 0;
    while (input.get(current_char))
    {
        std::string next_string = current_string + current_char;
        if (std::find(dictionary.begin(), dictionary.end(), next_string) != dictionary.end())
        {
            current_string = next_string;
        }
        else
        {
            int current_string_code = std::find(dictionary.begin(), dictionary.end(), current_string) - dictionary.begin();
            std::vector<bool> current_code_bits = Integer_To_Bits_(current_string_code, bits_per_code);
            binary_output.insert(binary_output.end(), current_code_bits.begin(), current_code_bits.end());
            std::cout << current_string_code << " ";
            output_count++;

            dictionary.push_back(next_string);
            bits_per_code = Required_No_Bits_(dictionary.size() - 1);

            current_string = std::string(1, current_char);
        }
        input_count++;
    }
    int current_string_code = std::find(dictionary.begin(), dictionary.end(), current_string) - dictionary.begin();
    std::cout << current_string_code << " ";
    std::vector<bool> current_code_bits = Integer_To_Bits_(current_string_code, bits_per_code);
    binary_output.insert(binary_output.end(), current_code_bits.begin(), current_code_bits.end());
    output_count++;
    std::cout << "\n";

    LZW lzw;
    lzw.bits_in_chars = BitsInChars(binary_output);
    for(int i = 0; i < original_dictionary.size(); i++)
        lzw.dictionary.push_back(original_dictionary[i][0]);

    std::cout << "DICCIONARIO\n";
    for(int i = 0; i < dictionary.size(); i++)
    {
        std::cout << i << ": " << dictionary[i] << ", ";
    }
    std::cout << "\n";

    std::cout << "BITS\n";
    Print_(lzw.bits_in_chars.To_Bits_(), std::cout, "", "", "");
    std::cout << '\n';

    return lzw;
}

void Save_(LZW lzw, std::ofstream& out)
{
    unsigned dictionary_size = lzw.dictionary.size();
    out.write((const char*)&dictionary_size, sizeof(dictionary_size));
    for(int i = 0; i < lzw.dictionary.size(); i++)
        out.write((const char*)&(lzw.dictionary[i]), sizeof(char));

    unsigned no_bits = lzw.bits_in_chars.no_bits;
    out.write((const char*)&no_bits, sizeof(no_bits));
    for(int i = 0; i < lzw.bits_in_chars.chars.size(); i++)
        out.write((const char*)&(lzw.bits_in_chars.chars[i]), 1);
}

LZW Load_(std::ifstream& in)
{
    LZW lzw;
    unsigned dictionary_size;
    in.read((char*)&dictionary_size, sizeof(dictionary_size));
    for(int i = 0; i < dictionary_size; i++)
    {
        char c;
        in.read(&c, 1);
        lzw.dictionary.push_back(c);
    }
    unsigned no_bits;
    in.read((char*)&no_bits, sizeof(no_bits));
    lzw.bits_in_chars.no_bits = no_bits;
    for(int i = 0; i < no_bits; i++)
    {
        char c;
        in.read(&c, 1);
        lzw.bits_in_chars.chars.push_back(c);
    }
    return lzw;
}

void Uncompress_(LZW lzw, std::ofstream& out)
{
    std::vector<std::string> dictionary;
    for(int i = 0; i < lzw.dictionary.size(); i++)
    {
        std::string str(1, lzw.dictionary[i]);
        dictionary.push_back(str);
    }
    unsigned bits_per_code = Required_No_Bits_(lzw.dictionary.size() - 1);
    std::vector<bool> binary = lzw.bits_in_chars.To_Bits_();
    std::cout << "binary " << binary.size() << "\n";
    Print_(binary, std::cout, "", "", "");
    std::cout << "\n";
    bool final_string_is_a_guess = false;
    std::string string = "";

    for(unsigned p = 0; p < binary.size();)
    {
        std::vector<bool> next_code_bits(binary.begin() + p, binary.begin() + p + bits_per_code);
        p += bits_per_code;
        unsigned next_code = Bits_To_Integer_(next_code_bits);

        std::cout << next_code << " ";

        if (next_code < dictionary.size())
            string = dictionary[next_code];
        else 
            string = string + string[0];

        if (final_string_is_a_guess)
            dictionary[dictionary.size() - 1] += string[0];

        out << string;

        dictionary.push_back(string);
        final_string_is_a_guess = true;
        bits_per_code = Required_No_Bits_(dictionary.size() - 1);
    }

    std::cout << "\n";
}
