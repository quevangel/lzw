#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <string>
#include <ostream>

#include <bits.hpp>
#include <print.hpp>
#include <lzw.hpp>

void Print_Help_();

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        Print_Help_();
    }
    else if (std::string(argv[1]) == "comprimir")
    {
        std::ifstream input(argv[2]);
        LZW lzw = Compress_(input);

        std::ofstream output(argv[3], std::ios::out | std::ios::binary);
        Save_(lzw, output);
    }
    else if (std::string(argv[1]) == "descomprimir")
    {
        std::ifstream input(argv[2], std::ios::in | std::ios::binary);
        LZW lzw = Load_(input);

        std::ofstream output(argv[3]);
        Uncompress_(lzw, output);
    }
    else
    {
        Print_Help_();
    }
    return 0;
}

void Print_Help_()
{
    std::cout << "Uso: lzw [comprimir/descomprimir] <archivo fuente> <archivo salida>\n";
}
