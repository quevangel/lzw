#pragma once

#include <iostream>

template<typename T, typename W = T>
static void Print_(std::vector<T> v, std::ostream& os = std::cout, std::string start = "(", std::string end = ")", std::string separator = ", ")
{
    os << start;
    for (typename std::vector<T>::iterator it = v.begin(); 
        it != v.end(); 
        it++)
    {
        if (it + 1 == v.end())
        {
            os << static_cast<W>(*it);
        }
        else
        {
            os << static_cast<W>(*it);
            os << separator;
        }
    }
    os << end;
    return;
}


