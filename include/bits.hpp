#pragma once

#include <vector>
#include <iostream>

std::vector<bool> Integer_To_Bits_(unsigned integer, unsigned no_bits);
unsigned Bits_To_Integer_(std::vector<bool> bits);
unsigned Required_No_Bits_(unsigned integer);

struct BitsInChars
{
    unsigned no_bits;
    std::vector<unsigned char> chars;
    unsigned ptr = 0;
    
    BitsInChars(std::vector<bool> bits);
    BitsInChars();
    std::vector<bool> To_Bits_();
    std::vector<bool> Get_Bits_(unsigned n);
};
