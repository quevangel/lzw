#pragma once

#include <vector>
#include <fstream>

#include <bits.hpp>

struct LZW
{
    std::vector<char> dictionary;
    BitsInChars bits_in_chars;
};

LZW Compress_(std::ifstream& input);
void Save_(LZW lzw, std::ofstream& output);
LZW Load_(std::ifstream& in);
void Uncompress_(LZW lzw, std::ofstream& out);


