#include <bits.hpp>

std::vector<bool> Integer_To_Bits_(unsigned integer, unsigned no_bits)
{
    std::vector<bool> bits;
    for (unsigned i = 0; i < no_bits; i++)
    {
        bits.push_back(integer % 2);
        integer >>= 1;
    }
    return bits;
}

unsigned Bits_To_Integer_(std::vector<bool> bits)
{
    if (bits.size() == 0)
    {
        return 0;
    }
    else
    {
        unsigned c = bits[0];
        return c + 2 * Bits_To_Integer_(std::vector<bool>(bits.begin() + 1, bits.end()));
    }
}

unsigned Required_No_Bits_(unsigned integer)
{
    unsigned required_no_bits = 0;
    while (integer)
    {
        integer >>= 1;
        required_no_bits++;
    }
    return required_no_bits;
}

BitsInChars::BitsInChars(std::vector<bool> bits)
{
    no_bits = bits.size();
    int i = 0;
    int remaining = no_bits - i;
    while (remaining > 8)
    {
        unsigned n = Bits_To_Integer_(std::vector<bool>(bits.begin() + i, bits.begin() + i + 8));
        chars.push_back(static_cast<unsigned char>(n));
        i += 8;
        remaining = no_bits - i;
    }
    unsigned n = Bits_To_Integer_(std::vector<bool>(bits.begin() + i, bits.end()));
    chars.push_back(static_cast<unsigned char>(n));
}

BitsInChars::BitsInChars()
{
    no_bits = 0;
}

std::vector<bool> BitsInChars::To_Bits_()
{
    std::vector<bool> bits;
    unsigned inserted = 0;
    for(unsigned i = 0; i < chars.size(); i++)
    {
        unsigned s_char = chars[i];
        std::vector<bool> b_char = Integer_To_Bits_(s_char, 8);
        for(unsigned j = 0; j < 8 && inserted < no_bits; j++)
        {
            bits.push_back(b_char[j]);
            inserted++;
        }
    }
    return bits;
}

std::vector<bool> BitsInChars::Get_Bits_(unsigned n)
{
    std::vector<bool> bits;
    for (unsigned i = 0; i < n; i++)
    {
        unsigned total_ptr = this->ptr + i;
        unsigned n_char = total_ptr / 8;
        unsigned n_bit = total_ptr % 8;

        if (total_ptr >= chars.size())
        {
            ptr = chars.size();
            ptr += i;
            return bits;
        }

        unsigned char c = chars[n_char];
        c >>= n_bit;
        bool val = c % 2;
        bits.push_back(val);
    }
    ptr += n;
    return bits;
}
